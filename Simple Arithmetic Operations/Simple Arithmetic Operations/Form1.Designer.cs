﻿namespace Simple_Arithmetic_Operations
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_Add = new System.Windows.Forms.Button();
            this.Btn_Quit = new System.Windows.Forms.Button();
            this.txt_FirstNumber = new System.Windows.Forms.TextBox();
            this.txt_SecondNumber = new System.Windows.Forms.TextBox();
            this.lbl_FirstSecond = new System.Windows.Forms.Label();
            this.lbl_SecondNumber = new System.Windows.Forms.Label();
            this.Btn_Subtraction = new System.Windows.Forms.Button();
            this.Btn_Multiply = new System.Windows.Forms.Button();
            this.Btn_Division = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btn_Add
            // 
            this.btn_Add.Location = new System.Drawing.Point(61, 578);
            this.btn_Add.Name = "btn_Add";
            this.btn_Add.Size = new System.Drawing.Size(234, 125);
            this.btn_Add.TabIndex = 0;
            this.btn_Add.Text = "Add";
            this.btn_Add.UseVisualStyleBackColor = true;
            this.btn_Add.Click += new System.EventHandler(this.btn_Add_Click);
            // 
            // Btn_Quit
            // 
            this.Btn_Quit.Location = new System.Drawing.Point(1045, 620);
            this.Btn_Quit.Name = "Btn_Quit";
            this.Btn_Quit.Size = new System.Drawing.Size(449, 140);
            this.Btn_Quit.TabIndex = 1;
            this.Btn_Quit.Text = "Quit";
            this.Btn_Quit.UseVisualStyleBackColor = true;
            this.Btn_Quit.Click += new System.EventHandler(this.Btn_Quit_Click);
            // 
            // txt_FirstNumber
            // 
            this.txt_FirstNumber.Location = new System.Drawing.Point(412, 184);
            this.txt_FirstNumber.Name = "txt_FirstNumber";
            this.txt_FirstNumber.Size = new System.Drawing.Size(323, 38);
            this.txt_FirstNumber.TabIndex = 2;
            // 
            // txt_SecondNumber
            // 
            this.txt_SecondNumber.Location = new System.Drawing.Point(412, 310);
            this.txt_SecondNumber.Name = "txt_SecondNumber";
            this.txt_SecondNumber.Size = new System.Drawing.Size(323, 38);
            this.txt_SecondNumber.TabIndex = 3;
            // 
            // lbl_FirstSecond
            // 
            this.lbl_FirstSecond.AutoSize = true;
            this.lbl_FirstSecond.Location = new System.Drawing.Point(83, 173);
            this.lbl_FirstSecond.Name = "lbl_FirstSecond";
            this.lbl_FirstSecond.Size = new System.Drawing.Size(177, 32);
            this.lbl_FirstSecond.TabIndex = 4;
            this.lbl_FirstSecond.Text = "First Number";
            // 
            // lbl_SecondNumber
            // 
            this.lbl_SecondNumber.AutoSize = true;
            this.lbl_SecondNumber.Location = new System.Drawing.Point(92, 310);
            this.lbl_SecondNumber.Name = "lbl_SecondNumber";
            this.lbl_SecondNumber.Size = new System.Drawing.Size(219, 32);
            this.lbl_SecondNumber.TabIndex = 5;
            this.lbl_SecondNumber.Text = "Second Number";
            // 
            // Btn_Subtraction
            // 
            this.Btn_Subtraction.Location = new System.Drawing.Point(313, 366);
            this.Btn_Subtraction.Name = "Btn_Subtraction";
            this.Btn_Subtraction.Size = new System.Drawing.Size(97, 394);
            this.Btn_Subtraction.TabIndex = 6;
            this.Btn_Subtraction.Text = "Subtraction";
            this.Btn_Subtraction.UseVisualStyleBackColor = true;
            // 
            // Btn_Multiply
            // 
            this.Btn_Multiply.Location = new System.Drawing.Point(485, 366);
            this.Btn_Multiply.Name = "Btn_Multiply";
            this.Btn_Multiply.Size = new System.Drawing.Size(753, 146);
            this.Btn_Multiply.TabIndex = 7;
            this.Btn_Multiply.Text = "Multiplication";
            this.Btn_Multiply.UseVisualStyleBackColor = true;
            // 
            // Btn_Division
            // 
            this.Btn_Division.Location = new System.Drawing.Point(433, 664);
            this.Btn_Division.Name = "Btn_Division";
            this.Btn_Division.Size = new System.Drawing.Size(467, 53);
            this.Btn_Division.TabIndex = 8;
            this.Btn_Division.Text = "Division";
            this.Btn_Division.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(16F, 31F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1575, 802);
            this.Controls.Add(this.Btn_Division);
            this.Controls.Add(this.Btn_Multiply);
            this.Controls.Add(this.Btn_Subtraction);
            this.Controls.Add(this.lbl_SecondNumber);
            this.Controls.Add(this.lbl_FirstSecond);
            this.Controls.Add(this.txt_SecondNumber);
            this.Controls.Add(this.txt_FirstNumber);
            this.Controls.Add(this.Btn_Quit);
            this.Controls.Add(this.btn_Add);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_Add;
        private System.Windows.Forms.Button Btn_Quit;
        private System.Windows.Forms.TextBox txt_FirstNumber;
        private System.Windows.Forms.TextBox txt_SecondNumber;
        private System.Windows.Forms.Label lbl_FirstSecond;
        private System.Windows.Forms.Label lbl_SecondNumber;
        private System.Windows.Forms.Button Btn_Subtraction;
        private System.Windows.Forms.Button Btn_Multiply;
        private System.Windows.Forms.Button Btn_Division;
    }
}

