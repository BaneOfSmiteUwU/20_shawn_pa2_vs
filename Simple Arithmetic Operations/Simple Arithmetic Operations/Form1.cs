﻿using System;
using System.Windows.Forms;

namespace Simple_Arithmetic_Operations {
    public partial class Form1 : Form {
        int FirstNumber, SecondNumber, Sum, Sub;

        private void btn_Add_Click(object sender, EventArgs e) {
            FirstNumber = int.Parse(txt_FirstNumber.Text);
            SecondNumber = int.Parse(txt_SecondNumber.Text);
            Sum = FirstNumber + SecondNumber;
            MessageBox.Show("The sum of the two numbers is : " + Sum.ToString());
        }

        private void Btn_Subtraction_Click(object sender, EventArgs e) {
            FirstNumber = int.Parse(txt_FirstNumber.Text);
            SecondNumber = int.Parse(txt_SecondNumber.Text);
            Sub = FirstNumber - SecondNumber;
            MessageBox.Show("The subtraction of the two numbers is : " + Sub.ToString());
        }

        private void Btn_Multiply_Click(object sender, EventArgs e) {
            FirstNumber = int.Parse(txt_FirstNumber.Text);
            SecondNumber = int.Parse(txt_SecondNumber.Text);
            MessageBox.Show("The Multiplication of the two numbers is : " + FirstNumber * SecondNumber);
        }

        private void Btn_Division_Click(object sender, EventArgs e) {
            FirstNumber = int.Parse(txt_FirstNumber.Text);
            SecondNumber = int.Parse(txt_SecondNumber.Text);
            MessageBox.Show("The Division of the two numbers is : " + FirstNumber / SecondNumber);
        }

        public Form1() {
            InitializeComponent();
        }

        private void Btn_Quit_Click(object sender, EventArgs e) {
            Application.Exit();
        }

        private void Form1_Load(object sender, EventArgs e) {

        }
    }
}
